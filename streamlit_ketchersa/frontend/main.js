import { Streamlit } from "streamlit-component-lib"

var old_value;

function onRender(event) {
  const data = event.detail
  document.getElementsByClassName('Ketcher-root')[0].parentNode.parentNode.parentNode.style = `height:${data.height}`

  old_value = ketcher.editor.historyStack.length

  setInterval(
    function () {
      if (old_value !== ketcher.editor.historyStack.length) {
        console.log('hello')
        old_value = ketcher.editor.historyStack.length
        ketcher.getSmiles().then( (smiles) => Streamlit.setComponentValue(smiles))
      }
    },
    500
  );


  Streamlit.setFrameHeight('630')
}

Streamlit.events.addEventListener(Streamlit.RENDER_EVENT, onRender)
Streamlit.setComponentReady()
Streamlit.setFrameHeight()